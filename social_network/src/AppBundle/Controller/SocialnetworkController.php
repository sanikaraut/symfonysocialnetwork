<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SocialnetworkController extends Controller
{
    /**
     * @Route("/social", name="social_login")
     */
    public function loginAction()
    { 
         $session = $request->getSession();

        // get the login error if there is one
         
        
        return $this->render('socialnetwork/index.html.twig');
    }
    /**
     * @Route("/social/search/{userName}", name="social_search")
     */
    public function searchAction($userName)
    { 
        return $this->render('socialnetwork/search.html.twig');
    }
     /**
     * @Route("/social/details", name="social_details")
     */
    public function detailsAction(Request $request)
    { 
        return $this->render('socialnetwork/details.html.twig');
    }
    /**
     * @Route("/social/logout", name="social_logout")
     */
    public function logoutAction(Request $request)
    { 
        return $this->render('socialnetwork/logout.html.twig');
    }
}
